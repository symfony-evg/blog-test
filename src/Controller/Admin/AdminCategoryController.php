<?php


namespace App\Controller\Admin;


use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCategoryController extends AdminBaseController
{
    /**
     * @Route("/admin/category/", name="admin_category")
     */
    public function index()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $forRender = parent::default();
        $forRender['title'] = 'Категории';
        $forRender['categories'] = $categories;
        return $this->render('admin/category/index.html.twig', $forRender);
    }

    /**
     * @Route("/admin/category/create/", name="admin_category_create")
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = new Category();
        $form = $this->createForm(CategoryType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setCreateAtValue();
            $post->setUpdateAtValue();
            $post->setIsPublished();
            $em->persist($post); // создаем объект в бд
            $em->flush();
            $this->addFlash('success', 'Категория добавлена'); // добавляем сообщение, что все ок

            return $this->redirectToRoute('admin_category');
        }

        $forRender = parent::default();
        $forRender['title'] = 'Создание категории';
        $forRender['form'] = $form->createView();

        return $this->render('admin/category/form.html.twig', $forRender);
    }

    /**
     * @Route("/admin/category/update/{id}/", name="admin_category_update")
     * @param int $id
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function update(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('save')->isClicked()) {
                $category->setUpdateAtValue();
                $this->addFlash('success', 'Категория обновлена'); // добавляем сообщение, что все ок
            }

            if ($form->get('delete')->isClicked()) {
                $em->remove($category);
                $this->addFlash('success', 'Категория удалена'); // добавляем сообщение, что все ок
            }

            $em->flush();

            return $this->redirectToRoute('admin_category');
        }

        $forRender = parent::default();
        $forRender['title'] = 'Обновление категории';
        $forRender['form'] = $form->createView();

        return $this->render('admin/category/form.html.twig', $forRender);
    }
}
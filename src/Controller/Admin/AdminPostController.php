<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Post;
use App\Form\PostType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminPostController extends AdminBaseController
{
    /**
     * @Route("/admin/post/", name="admin_post")
     */
    public function index()
    {
        $posts = $this->getDoctrine()->getRepository(Post::class)->findAll();
        $category = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $forRender = parent::default();
        $forRender['title'] = 'Посты';
        $forRender['posts'] = $posts;
        $forRender['category'] = $category;
        return $this->render('admin/post/index.html.twig', $forRender);
    }

    /**
     * @Route("/admin/post/create/", name="admin_post_create")
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setCreateAtValue();
            $post->setUpdateAtValue();
            $post->setIsPublished();
            $em->persist($post); // создаем объект в бд
            $em->flush();
            $this->addFlash('success', 'Пост добавлен'); // добавляем сообщение, что все ок

            return $this->redirectToRoute('admin_post');
        }

        $forRender = parent::default();
        $forRender['title'] = 'Создание поста';
        $forRender['form'] = $form->createView();

        return $this->render('admin/post/form.html.twig', $forRender);
    }


    /**
     * @Route("/admin/post/update/{id}/", name="admin_post_update")
     * @param int $id
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function update(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('save')->isClicked()) {
                $post->setUpdateAtValue();
                $this->addFlash('success', 'Пост обновлен'); // добавляем сообщение, что все ок
            }

            if ($form->get('delete')->isClicked()) {
                $em->remove($post);
                $this->addFlash('success', 'Пост удален'); // добавляем сообщение, что все ок
            }

            $em->flush();

            return $this->redirectToRoute('admin_post');
        }

        $forRender = parent::default();
        $forRender['title'] = 'Обновление поста';
        $forRender['form'] = $form->createView();

        return $this->render('admin/post/form.html.twig', $forRender);
    }
}